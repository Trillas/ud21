package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.JList;

public class views extends JFrame {
	
	
	private JLabel lblResultado = new JLabel(" ");
	private JLabel lblResultadoAux = new JLabel(" ");
	private static double num1;
	private static double num2;
	private static double resultado;
	private static String operador;
	
	public views() {
		getContentPane().setLayout(null);
		
		//-----BOTONES-----
		JButton btn7 = new JButton("7");
		btn7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 7);
			}
		});
		btn7.setBounds(25, 258, 60, 50);
		getContentPane().add(btn7);
		
		
		JButton btn8 = new JButton("8");
		btn8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 8);
			}
		});
		btn8.setBounds(95, 258, 60, 50);
		getContentPane().add(btn8);
		
		
		JButton btn9 = new JButton("9");
		btn9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 9);
			}
		});
		btn9.setBounds(165, 258, 60, 50);
		getContentPane().add(btn9);
		
		
		JButton btn4 = new JButton("4");
		btn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 4);
			}
		});
		btn4.setBounds(25, 319, 60, 50);
		getContentPane().add(btn4);
		
		
		JButton btn5 = new JButton("5");
		btn5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 5);
			}
		});
		btn5.setBounds(95, 319, 60, 50);
		getContentPane().add(btn5);
		
		
		JButton btn6 = new JButton("6");
		btn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 6);
			}
		});
		btn6.setBounds(165, 319, 60, 50);
		getContentPane().add(btn6);
		
		
		JButton btn1 = new JButton("1");
		btn1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 1);
			}
		});
		btn1.setBounds(25, 380, 60, 50);
		getContentPane().add(btn1);
		
		
		JButton btn2 = new JButton("2");
		btn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 2);
			}
		});
		btn2.setBounds(95, 380, 60, 50);
		getContentPane().add(btn2);
		
		
		JButton btn3 = new JButton("3");
		btn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 3);
			}
		});
		btn3.setBounds(165, 380, 60, 50);
		getContentPane().add(btn3);
		
		
		JButton btn0 = new JButton("0");
		btn0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblResultado.setText(lblResultado.getText() + 0);
			}
		});
		btn0.setBounds(25, 438, 130, 50);
		getContentPane().add(btn0);
		lblResultado.setHorizontalAlignment(SwingConstants.LEFT);
		
		//-----Label de resultado-----
		lblResultado.setBounds(25, 83, 262, 42);
		getContentPane().add(lblResultado);
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 614, 553);
		setVisible(true);
		
		final JTextArea textHistorial = new JTextArea();
		textHistorial.setBounds(316, 53, 254, 434);
		getContentPane().add(textHistorial);
		//-----Operaciones-----
		JButton btnDividido1 = new JButton("1/x");
		btnDividido1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  1 / Double.parseDouble(lblResultado.getText());
				textHistorial.setText(textHistorial.getText() + ("1/" + lblResultado.getText() + "=" + num2 + "\n"));
				lblResultadoAux.setText(String.valueOf(num2));
				lblResultado.setText(String.valueOf(num2));

			}
		});
		btnDividido1.setBounds(25, 197, 60, 50);
		getContentPane().add(btnDividido1);
		
		JButton btnElevado2 = new JButton("x2");
		btnElevado2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  Double.parseDouble(lblResultado.getText()) * Double.parseDouble(lblResultado.getText());
				textHistorial.setText(textHistorial.getText() + (lblResultado.getText() + "x" + lblResultado.getText() + "=" + num2 + "\n"));
				lblResultadoAux.setText(String.valueOf(num2));
				lblResultado.setText(String.valueOf(num2));

			}
		});
		btnElevado2.setBounds(95, 197, 60, 50);
		getContentPane().add(btnElevado2);
		
		JButton btnRaiz = new JButton("√x");
		btnRaiz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2 =  Math.sqrt(Double.parseDouble(lblResultado.getText()));
				textHistorial.setText(textHistorial.getText() + ("√" + lblResultado.getText() + "=" + num2 + "\n"));
				lblResultadoAux.setText(String.valueOf(num2));
				lblResultado.setText(String.valueOf(num2));

			}
		});
		btnRaiz.setBounds(165, 197, 60, 50);
		getContentPane().add(btnRaiz);
		
		JButton btnMultiplicacion = new JButton("X");
		btnMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "x";
				num2 =  Double.parseDouble(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "x");
				lblResultado.setText(" ");

			}
		});
		btnMultiplicacion.setBounds(237, 258, 60, 50);
		getContentPane().add(btnMultiplicacion);
		
		JButton btnResta = new JButton("-");
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "-";
				num2 =  Double.parseDouble(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "-");
				lblResultado.setText(" ");
			}
		});
		btnResta.setBounds(238, 319, 59, 50);
		getContentPane().add(btnResta);
		
		JButton btnPunto = new JButton(".");
		btnPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText(lblResultado.getText() + ".");

			}
		});
		btnPunto.setBounds(165, 438, 60, 50);
		getContentPane().add(btnPunto);
		
		JButton btnSuma = new JButton("+");
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "+";
				num2 =  Double.parseDouble(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "+");
				lblResultado.setText(" ");

			}
		});
		btnSuma.setBounds(237, 380, 60, 50);
		getContentPane().add(btnSuma);
		
		JButton btnDivision = new JButton("/");
		btnDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "/";
				num2 =  Double.parseDouble(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "/");
				lblResultado.setText(" ");

			}
		});
		btnDivision.setBounds(237, 197, 60, 50);
		getContentPane().add(btnDivision);
		
		JButton btnPercen = new JButton("%");
		btnPercen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				operador = "%";
				num2 =  Double.parseDouble(lblResultado.getText());
				lblResultadoAux.setText(lblResultado.getText() + "%");
				lblResultado.setText(" ");

			}
		});
		btnPercen.setBounds(25, 136, 60, 50);
		getContentPane().add(btnPercen);
		
		JButton btnCE = new JButton("CE");
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText(" ");
				num1 = 0;

			}
		});
		btnCE.setBounds(95, 136, 60, 50);
		getContentPane().add(btnCE);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText(" ");
				lblResultadoAux.setText(" ");
				num1 = 0;
				num2 = 0;
			}
		});
		btnC.setBounds(165, 136, 60, 50);
		getContentPane().add(btnC);
		
		JButton btnDelete = new JButton("Del");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText(""+lblResultado.getText().substring(0, lblResultado.getText().length() - 1));
			}
		});
		btnDelete.setBounds(237, 136, 60, 50);
		getContentPane().add(btnDelete);
		

		JButton btnIgual = new JButton("=");
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1 = Double.parseDouble(lblResultado.getText());
				if (operador.contentEquals("+")) {
					resultado = num2 + num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "+" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("-")) {
					resultado = num2 - num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "-" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("/")) {
					resultado = num2 / num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "/" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("x")) {
					resultado = num2 * num1;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "x" + num1 + "=" + resultado + "\n"));
				}else if (operador.contentEquals("%")) {
					resultado = (num1/100) * num2;
					lblResultado.setText(String.valueOf(resultado));
					lblResultadoAux.setText(String.valueOf(resultado));
					textHistorial.setText(textHistorial.getText() + (num2 + "%" + num1 + "=" + resultado + "\n"));
				}
			}
		});
		btnIgual.setBounds(237, 438, 60, 50);
		getContentPane().add(btnIgual);
		
		//-----Label de historial-----
		JLabel lblEnunciado = new JLabel("Resultado");
		lblEnunciado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEnunciado.setBounds(25, 26, 87, 19);
		getContentPane().add(lblEnunciado);
		
		JLabel lblHistorial = new JLabel("Historial");
		lblHistorial.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHistorial.setBounds(316, 26, 87, 19);
		getContentPane().add(lblHistorial);
		
		lblResultadoAux = new JLabel(" ");
		lblResultadoAux.setHorizontalAlignment(SwingConstants.LEFT);
		lblResultadoAux.setBounds(25, 56, 262, 19);
		getContentPane().add(lblResultadoAux);
		
		
	}
}
